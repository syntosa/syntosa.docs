# Syntosa.Core.ObjectModel.EdgeElementGlobalProperty
** need class/table description **
DataTable: EDGE_ELEMENT_GLOBALPROPERTY_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|LeftRecordType|** need property description **|SyntosaRecordType|True|False|
|LeftUId|** need property description **|Guid|True|True|
|ElementUId|** need property description **|Guid|True|True|
|IsElementActive|** need property description **|Boolean|True|False|
|ElementName|** need property description **|String|True|False|
|ElementTypeUId|** need property description **|Guid|True|False|
|ElementTypeName|** need property description **|String|True|False|
|IsElementTypeActive|** need property description **|Boolean|True|False|
|RightRecordType|** need property description **|SyntosaRecordType|True|False|
|RightUId|** need property description **|Guid|True|True|
|GlobalPropertyUId|** need property description **|Guid|True|True|
|GlobalPropertyTypeUId|** need property description **|Guid|True|False|
|GlobalPropertyTypeName|** need property description **|String|True|False|
|GlobalPropertyElementName|** need property description **|String|True|False|
|IsGlobalPropertyElementActive|** need property description **|Boolean|True|False|
|GlobalPropertyElementTypeUId|** need property description **|Guid|True|False|
|IsLeftUIdSource|** need property description **|Boolean|True|True|
|IsElementDriver|** need property description **|Boolean|True|True|
|EdgeProperties|** need property description **|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|** need property description **|IList|True|False|
|GlobalAttribute|** need property description **|String|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|IsTypeItemActive|** need property description **|Boolean|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_GLOBALPROPERTY_XREF_PK|OneWayFromSourceRequired|Pk|edgeElementGlobalId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_GLOBALPROPERTY_XREF_UID|TwoWayRequired|UId|edgeElementGlobalUId|SqlDbType.UniqueIdentifier|False|
|LeftRecordType|None|None|Unknown|-|-|N/A|
|LeftUId|None|None|Unknown|-|-|N/A|
|ElementUId|ELEMENT_UID|TwoWayRequired|Unknown|elementUId|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|ElementTypeUId|TYPE_UID_ELEMENT|OneWayFromSourceRequired|Unknown|elementTypeUId|-|False|
|ElementTypeName|TYPE_NAME_ELEMENT|OneWayFromSourceRequired|Unknown|elementTypeName|-|False|
|IsElementTypeActive|ISACTIVE_ELEMENT_TYPE|OneWayFromSourceRequired|Unknown|isElementTypeActive|-|False|
|RightRecordType|None|None|Unknown|-|-|N/A|
|RightUId|None|None|Unknown|-|-|N/A|
|GlobalPropertyUId|ELEMENT_GLOBALPROPERTY_UID|TwoWayRequired|Unknown|globalPropertyUId|-|False|
|GlobalPropertyTypeUId|TYPE_UID_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|globalPropertyTypeUId|-|False|
|GlobalPropertyTypeName|TYPE_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|globalPropertyTypeName|-|False|
|GlobalPropertyElementName|ELEMENT_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|globalPropertyElementName|-|False|
|IsGlobalPropertyElementActive|ISACTIVE_GLOBALPROPERTY_ELEMENT|OneWayFromSourceRequired|Unknown|isGlobalPropertyElementActive|-|False|
|GlobalPropertyElementTypeUId|TYPE_UID_GLOBALPROPERTY_ELEMENT|OneWayFromSourceRequired|Unknown|globalPropertyElementTypeUId|-|False|
|IsLeftUIdSource|None|None|Unknown|-|-|N/A|
|IsElementDriver|ELEMENT_UID_DRIVER|TwoWayRequired|Unknown|isElementDriver|-|False|
|EdgeProperties|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|None|None|Unknown|-|-|N/A|
|GlobalAttribute|GLOBAL_ATTRIBUTE|OneWayFromSourceRequired|Unknown|globalAttribute|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE|OneWayFromSourceRequired|Name|typeItemName|-|False|
|IsTypeItemActive|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isTypeItemActive|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
