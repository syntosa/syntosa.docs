# Syntosa.Core.ObjectModel.Domain
** need class/table description **
DataTable: DOMAIN

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this Domain.|Int32|True|True|
|UId|The unique identifier of this Domain.|Guid|True|True|
|Name|The name of this Domain.|String|True|True|
|Description|The friendly description of this Domain.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|IsBuiltIn|Indicates items which are integral to system function.  IsBuiltIn=true items cannot be deleted.|Boolean|True|True|
|ParentUId|Self-referencing foreign key to the parent Domain for this Domain.|Guid|True|True|
|ParentName|** need property description **|String|True|False|
|IsParentActive|** need property description **|Boolean|True|False|
|Children|A list of the child Domains of this Domain.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|RlsOwner|The row-level security Owner UId.|Guid|True|True|
|RlsMask|The row-level security bitmask for this Domain.|Byte[]|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|DOMAIN_PK|OneWayFromSourceRequired|Pk|domainId|SqlDbType.Int|False|
|UId|DOMAIN_UID|TwoWayRequired|UId|domainUId|SqlDbType.UniqueIdentifier|False|
|Name|DOMAIN_NAME|TwoWayRequired|Name|domainName|-|False|
|Description|DOMAIN_DESC|TwoWayRequired|Unknown|domainDesc|-|False|
|IsActive|ISACTIVE|TwoWayRequired|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayRequired|Unknown|isBuiltIn|-|False|
|ParentUId|DOMAIN_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|ParentName|DOMAIN_NAME_PARENT|OneWayFromSourceRequired|Unknown|parentName|-|False|
|IsParentActive|ISACTIVE_DOMAIN_PARENT|OneWayFromSourceRequired|Unknown|isParentActive|-|False|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|RlsOwner|RLS_OWNER|TwoWayRequired|Unknown|rlsOwner|-|True|
|RlsMask|RLS_MASK|TwoWayRequired|Unknown|rlsMask|-|True|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
