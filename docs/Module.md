# Syntosa.Core.ObjectModel.Module
** need class/table description **
DataTable: MODULE

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this Module.|Int32|True|True|
|UId|The unique identifier of this Module.|Guid|True|True|
|Name|The name of this Module.|String|True|True|
|Description|The friendly description of this Module.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|IsBuiltIn|Indicates items which are integral to system function.  IsBuiltIn=true items cannot be deleted.|Boolean|True|True|
|ParentUId|Self-referencing foreign key to the parent Module for this Module.|Guid|True|True|
|ParentName|** need property description **|String|True|False|
|IsParentActive|** need property description **|Boolean|True|False|
|Children|A list of the child Modules of this Module.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|MODULE_PK|OneWayFromSourceRequired|Pk|moduleId|SqlDbType.Int|False|
|UId|MODULE_UID|TwoWayRequired|UId|moduleUId|SqlDbType.UniqueIdentifier|False|
|Name|MODULE_NAME|TwoWayRequired|Name|moduleName|-|False|
|Description|MODULE_DESC|TwoWayRequired|Unknown|moduleDesc|-|False|
|IsActive|ISACTIVE|TwoWayRequired|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayRequired|Unknown|isBuiltIn|-|False|
|ParentUId|MODULE_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|ParentName|MODULE_NAME_PARENT|OneWayFromSourceRequired|Unknown|parentName|-|False|
|IsParentActive|ISACTIVE_MODULE_PARENT|OneWayFromSourceRequired|Unknown|isParentActive|-|False|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
