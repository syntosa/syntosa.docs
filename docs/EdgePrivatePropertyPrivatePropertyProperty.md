# Syntosa.Core.ObjectModel.EdgePrivatePropertyPrivatePropertyProperty
** need class/table description **
DataTable: EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|EdgeType|** need property description **|SyntosaRecordType|True|False|
|EdgeUId|** need property description **|Guid|True|True|
|IsActiveEdgeProperty|** need property description **|Boolean|True|False|
|ElementPrivatePropertyUIdSource|** need property description **|Guid|True|False|
|ElementPrivatePropertyUIdTarget|** need property description **|Guid|True|False|
|TypeNameEdge|** need property description **|String|True|False|
|IsActiveTypeEdge|** need property description **|Boolean|True|False|
|ElementPrivatePropertyKeyUIdSource|** need property description **|Guid|True|False|
|IsActiveElementPrivatePropertyKeySource|** need property description **|Boolean|True|False|
|PrivateAttributeSource|** need property description **|String|True|False|
|ElementPrivatePropertyKeyUIdTarget|** need property description **|Guid|True|False|
|IsActiveElementPrivatePropertyKeyTarget|** need property description **|Boolean|True|False|
|PrivateAttributeTarget|** need property description **|String|True|False|
|TypeUIdKeySource|** need property description **|Guid|True|False|
|TypeNameKeySource|** need property description **|String|True|False|
|IsActiveTypeKeySource|** need property description **|Boolean|True|False|
|TypeUIdValueSource|** need property description **|Guid|True|False|
|TypeNameValueSource|** need property description **|String|True|False|
|IsActiveValueKeySource|** need property description **|Boolean|True|False|
|TypeUIdKeyTarget|** need property description **|Guid|True|False|
|TypeNameKeyTarget|** need property description **|String|True|False|
|IsActiveTypeKeyTarget|** need property description **|Boolean|True|False|
|TypeUIdValueTarget|** need property description **|Guid|True|False|
|TypeNameValueTarget|** need property description **|String|True|False|
|IsActiveTypeValueTarget|** need property description **|Boolean|True|False|
|TypeUnitUIdSource|** need property description **|Guid|True|False|
|IsActiveTypeUnitSource|** need property description **|Boolean|True|False|
|TypeUnitUIdTarget|** need property description **|Guid|True|False|
|IsActiveTypeUnitTarget|** need property description **|Boolean|True|False|
|ElementUIdSource|** need property description **|Guid|True|False|
|ElementNameSource|** need property description **|String|True|False|
|IsActiveElementSource|** need property description **|Boolean|True|False|
|TypeUIdElementSource|** need property description **|Guid|True|False|
|ElementUIdTarget|** need property description **|Guid|True|False|
|ElementNameTarget|** need property description **|String|True|False|
|IsActiveElementTarget|** need property description **|Boolean|True|False|
|TypeUIdElementTarget|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|Attribute|** need property description **|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_PK|OneWayFromSourceRequired|Pk|edgePrivatePrivatePropId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID|TwoWayRequired|UId|edgePrivatePrivatePropUId|SqlDbType.UniqueIdentifier|False|
|EdgeType|None|None|Unknown|-|-|N/A|
|EdgeUId|EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF_UID|TwoWayRequired|Unknown|edgeUId|-|False|
|IsActiveEdgeProperty|ISACTIVE_EDGE_PROPERTY|OneWayFromSourceRequired|Unknown|isActiveEdgeProperty|-|False|
|ElementPrivatePropertyUIdSource|ELEMENT_PRIVATEPROPERTY_UID_SOURCE|OneWayFromSourceRequired|Unknown|elementPrivatePropertyUIdSource|-|False|
|ElementPrivatePropertyUIdTarget|ELEMENT_PRIVATEPROPERTY_UID_TARGET|OneWayFromSourceRequired|Unknown|elementPrivatePropertyUIdTarget|-|False|
|TypeNameEdge|TYPE_NAME_EDGE|OneWayFromSourceRequired|Unknown|typeNameEdge|-|False|
|IsActiveTypeEdge|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isActiveTypeEdge|-|False|
|ElementPrivatePropertyKeyUIdSource|ELEMENT_PRIVATEPROPERTY_KEY_UID_SOURCE|OneWayFromSourceRequired|Unknown|elementPrivatePropertyKeyUIdSource|-|False|
|IsActiveElementPrivatePropertyKeySource|ISACTIVE_ELEMENT_PRIVATEPROPERTY_KEY_SOURCE|OneWayFromSourceRequired|Unknown|isActiveElementPrivatePropertyKeySource|-|False|
|PrivateAttributeSource|PRIVATE_ATTRIBUTE_SOURCE|OneWayFromSourceRequired|Unknown|privateAttributeSource|-|False|
|ElementPrivatePropertyKeyUIdTarget|ELEMENT_PRIVATEPROPERTY_KEY_UID_TARGET|OneWayFromSourceRequired|Unknown|elementPrivatePropertyKeyUIdTarget|-|False|
|IsActiveElementPrivatePropertyKeyTarget|ISACTIVE_ELEMENT_PRIVATEPROPERTY_KEY_TARGET|OneWayFromSourceRequired|Unknown|isActiveElementPrivatePropertyKeyTarget|-|False|
|PrivateAttributeTarget|PRIVATE_ATTRIBUTE_TARGET|OneWayFromSourceRequired|Unknown|privateAttributeTarget|-|False|
|TypeUIdKeySource|TYPE_UID_KEY_SOURCE|OneWayFromSourceRequired|Unknown|typeUIdKeySource|-|False|
|TypeNameKeySource|TYPE_NAME_KEY_SOURCE|OneWayFromSourceRequired|Unknown|typeNameKeySource|-|False|
|IsActiveTypeKeySource|ISACTIVE_TYPE_KEY_SOURCE|OneWayFromSourceRequired|Unknown|isActiveTypeKeySource|-|False|
|TypeUIdValueSource|TYPE_UID_VALUE_SOURCE|OneWayFromSourceRequired|Unknown|typeUIdValueSource|-|False|
|TypeNameValueSource|TYPE_NAME_VALUE_SOURCE|OneWayFromSourceRequired|Unknown|typeNameValueSource|-|False|
|IsActiveValueKeySource|ISACTIVE_VALUE_KEY_SOURCE|OneWayFromSourceRequired|Unknown|isActiveValueKeySource|-|False|
|TypeUIdKeyTarget|TYPE_UID_KEY_TARGET|OneWayFromSourceRequired|Unknown|typeUIdKeyTarget|-|False|
|TypeNameKeyTarget|TYPE_NAME_KEY_TARGET|OneWayFromSourceRequired|Unknown|typeNameKeyTarget|-|False|
|IsActiveTypeKeyTarget|ISACTIVE_TYPE_KEY_TARGET|OneWayFromSourceRequired|Unknown|isActiveTypeKeyTarget|-|False|
|TypeUIdValueTarget|TYPE_UID_VALUE_TARGET|OneWayFromSourceRequired|Unknown|typeUIdValueTarget|-|False|
|TypeNameValueTarget|TYPE_NAME_VALUE_TARGET|OneWayFromSourceRequired|Unknown|typeNameValueTarget|-|False|
|IsActiveTypeValueTarget|ISACTIVE_TYPE_VALUE_TARGET|OneWayFromSourceRequired|Unknown|isActiveTypeValueTarget|-|False|
|TypeUnitUIdSource|TYPE_UNIT_UID_SOURCE|OneWayFromSourceRequired|Unknown|typeUnitUIdSource|-|False|
|IsActiveTypeUnitSource|ISACTIVE_TYPE_UNIT_SOURCE|OneWayFromSourceRequired|Unknown|isActiveTypeUnitSource|-|False|
|TypeUnitUIdTarget|TYPE_UNIT_UID_TARGET|OneWayFromSourceRequired|Unknown|typeUnitUIdTarget|-|False|
|IsActiveTypeUnitTarget|ISACTIVE_TYPE_UNIT_TARGET|OneWayFromSourceRequired|Unknown|isActiveTypeUnitTarget|-|False|
|ElementUIdSource|ELEMENT_UID_SOURCE|OneWayFromSourceRequired|Unknown|elementUIdSource|-|False|
|ElementNameSource|ELEMENT_NAME_SOURCE|OneWayFromSourceRequired|Unknown|elementNameSource|-|False|
|IsActiveElementSource|ISACTIVE_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|isActiveElementSource|-|False|
|TypeUIdElementSource|TYPE_UID_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|typeUIdElementSource|-|False|
|ElementUIdTarget|ELEMENT_UID_TARGET|OneWayFromSourceRequired|Unknown|elementUIdTarget|-|False|
|ElementNameTarget|ELEMENT_NAME_TARGET|OneWayFromSourceRequired|Unknown|elementNameTarget|-|False|
|IsActiveElementTarget|ISACTIVE_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|isActiveElementTarget|-|False|
|TypeUIdElementTarget|TYPE_UID_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|typeUIdElementTarget|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE_PROPERTY|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE_PROPERTY|OneWayFromSourceRequired|Name|typeItemName|-|False|
|Attribute|EDGE_PROPERTY_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
