# Syntosa.Core.ObjectModel.TypeXref
** need class/table description **
DataTable: TYPE_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this TypeXref.|Int32|True|True|
|UId|The unique identifier of this TypeXref.|Guid|True|True|
|ParentTypeUId|** need property description **|Guid|True|True|
|ParentTypeName|** need property description **|String|True|False|
|IsParentTypeActive|** need property description **|Boolean|True|False|
|ChildTypeUId|** need property description **|Guid|True|True|
|ChildTypeName|** need property description **|String|True|False|
|IsChildTypeActive|** need property description **|Boolean|True|False|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|TYPE_XREF_PK|OneWayFromSourceRequired|Pk|typeXrefId|SqlDbType.Int|False|
|UId|TYPE_XREF_UID|TwoWayRequired|UId|typeXrefUId|SqlDbType.UniqueIdentifier|False|
|ParentTypeUId|TYPE_UID_PARENT|TwoWayRequired|Unknown|parentTypeUId|-|False|
|ParentTypeName|TYPE_NAME_PARENT|OneWayFromSourceRequired|Name|parentTypeName|-|False|
|IsParentTypeActive|ISACTIVE_TYPE_PARENT|OneWayFromSourceRequired|Unknown|isParentTypeActive|-|False|
|ChildTypeUId|TYPE_UID_CHILD|TwoWayRequired|Unknown|childTypeUId|-|False|
|ChildTypeName|TYPE_NAME_CHILD|OneWayFromSourceRequired|Unknown|childTypeName|-|False|
|IsChildTypeActive|ISACTIVE_TYPE_CHILD|OneWayFromSourceRequired|Unknown|isChildTypeActive|-|False|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|Name|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
