# Syntosa.Core.ObjectModel.EdgeElementElementProperty
** need class/table description **
DataTable: EDGE_ELEMENT_ELEMENT_XREF_PROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|EdgeType|** need property description **|SyntosaRecordType|True|False|
|EdgeUId|** need property description **|Guid|True|True|
|IsEdgePropertyActive|** need property description **|Boolean|True|False|
|SourceElementUId|** need property description **|Guid|True|False|
|TargetElementUId|** need property description **|Guid|True|False|
|EdgeTypeUId|** need property description **|Guid|True|False|
|EdgeTypeName|** need property description **|String|True|False|
|IsEdgeTypeActive|** need property description **|Boolean|True|False|
|SourceElementName|** need property description **|String|True|False|
|IsSourceElementActive|** need property description **|Boolean|True|False|
|TypeSourceElementUId|** need property description **|Guid|True|False|
|TargetElementName|** need property description **|String|True|False|
|IsTargetElementActive|** need property description **|Boolean|True|False|
|TargetElementTypeUId|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|Attribute|** need property description **|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_ELEMENT_XREF_PROPERTY_PK|OneWayFromSourceRequired|Pk|edgeElementElementId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_ELEMENT_XREF_PROPERTY_UID|TwoWayRequired|UId|edgeElementElementUId|SqlDbType.UniqueIdentifier|False|
|EdgeType|None|None|Unknown|-|-|N/A|
|EdgeUId|EDGE_ELEMENT_ELEMENT_XREF_UID|TwoWayRequired|Unknown|edgeUId|-|False|
|IsEdgePropertyActive|ISACTIVE_EDGE_PROPERTY|OneWayFromSourceRequired|Unknown|isEdgePropertyActive|-|False|
|SourceElementUId|ELEMENT_UID_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementUId|-|False|
|TargetElementUId|ELEMENT_UID_TARGET|OneWayFromSourceRequired|Unknown|targetElementUId|-|False|
|EdgeTypeUId|TYPE_UID_EDGE|OneWayFromSourceRequired|Unknown|edgeTypeUId|-|False|
|EdgeTypeName|TYPE_NAME_EDGE|OneWayFromSourceRequired|Unknown|edgeTypeName|-|False|
|IsEdgeTypeActive|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isEdgeTypeActive|-|False|
|SourceElementName|ELEMENT_NAME_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementName|-|False|
|IsSourceElementActive|ISACTIVE_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|isSourceElementActive|-|False|
|TypeSourceElementUId|TYPE_UID_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|typeSourceElementUId|-|False|
|TargetElementName|ELEMENT_NAME_TARGET|OneWayFromSourceRequired|Unknown|targetElementName|-|False|
|IsTargetElementActive|ISACTIVE_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|isTargetElementActive|-|False|
|TargetElementTypeUId|TYPE_UID_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|targetElementTypeUId|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE_PROPERTY|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE_PROPERTY|OneWayFromSourceRequired|Name|typeItemName|-|False|
|Attribute|EDGE_PROPERTY_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
