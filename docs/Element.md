# Syntosa.Core.ObjectModel.Element
** need class/table description **
DataTable: ELEMENT

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this Element.|Int32|True|True|
|UId|The unique identifier of this Element.|Guid|True|True|
|Name|The name of this Element.|String|True|True|
|Description|The friendly description of this Element.|String|True|True|
|IsActive|** need property description **|Boolean|True|True|
|IsBuiltIn|** need property description **|Boolean|True|True|
|Alias|A secondary friendly name for this element.|String|True|True|
|IsAutoCollect|Indicates if element data is collected though manual input (AutoCollect=false) or automated processes (AutoCollect=true).|Boolean|True|True|
|ModuleUId|Foreign key to Module table; the Module for this Element.|Guid|True|True|
|ModuleName|** need property description **|String|True|False|
|IsModuleActive|** need property description **|Boolean|True|False|
|ModuleRecordKey|*** ?? ***|String|True|True|
|TypeUIdRecordStatus|*** Foreign key to Type table where TypeFunction is RecordStatus.|Guid|True|True|
|DomainUId|Foreign key to Domain table; the Domain for this Element.|Guid|True|True|
|DomainName|** need property description **|String|True|False|
|IsDomainActive|** need property description **|Boolean|True|False|
|TypeItemUId|Foreign key to Type table; the Type of this Element.|Guid|True|True|
|TypeItemName|** need property description **|String|True|False|
|IsTypeItemActive|** need property description **|Boolean|True|False|
|TypeFunctionName|** need property description **|String|True|False|
|IsTypeFunctionActive|** need property description **|Boolean|True|False|
|ParentUId|Self-referencing foreign key to the parent Element for this Element.|Guid|True|True|
|ParentName|** need property description **|String|True|False|
|IsParentActive|** need property description **|Boolean|True|False|
|Children|A list of the child Elements of this Element.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|RlsOwner|The row-level security Owner UId.|Guid|True|True|
|RlsMask|The row-level security bitmask for this Element.|Byte[]|True|True|
|RecordStatus|** need property description **|String|True|False|
|GlobalProperties|A List of the GlobalProperty items for this Element.|Dictionary`2|True|True|
|PrivatePropertyKeys|A List of the PrivateProperty items for this Element.|List`1|True|True|
|PrivateProperties|A List of the PrivateProperty items for this Element.|List`1|True|True|
|ElementEdges|** need property description **|List`1|True|True|
|GlobalPropertyEdges|** need property description **|List`1|True|True|
|PrivatePropertyEdges|** need property description **|List`1|True|True|
|LabelEdges|** need property description **|List`1|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|ELEMENT_PK|OneWayFromSourceRequired|Pk|elementId|SqlDbType.Int|False|
|UId|ELEMENT_UID|TwoWayRequired|UId|elementUId|SqlDbType.UniqueIdentifier|False|
|Name|ELEMENT_NAME|TwoWayRequired|Name|elementName|-|False|
|Description|ELEMENT_DESC|TwoWayRequired|Unknown|elementDesc|-|False|
|IsActive|ISACTIVE|TwoWayOptional|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayOptional|Unknown|isBuiltIn|-|False|
|Alias|ELEMENT_ALIAS|TwoWayRequired|Unknown|alias|-|False|
|IsAutoCollect|ISAUTOCOLLECT|TwoWayRequired|Unknown|isAutoCollect|-|False|
|ModuleUId|MODULE_UID|TwoWayRequired|Unknown|moduleUId|-|False|
|ModuleName|MODULE_NAME|OneWayFromSourceRequired|Unknown|moduleName|-|False|
|IsModuleActive|ISACTIVE_MODULE|OneWayFromSourceRequired|Unknown|isModuleActive|-|False|
|ModuleRecordKey|MODULE_RECORD_PK|TwoWayRequired|Unknown|moduleRecordKey|-|False|
|TypeUIdRecordStatus|TYPE_UID_RECORD_STATUS|TwoWayRequired|Unknown|typeUIdRecordStatus|-|False|
|DomainUId|DOMAIN_UID|TwoWayRequired|Unknown|domainUId|-|False|
|DomainName|DOMAIN_NAME|OneWayFromSourceRequired|Unknown|domainName|-|False|
|IsDomainActive|ISACTIVE_DOMAIN|OneWayFromSourceRequired|Unknown|isDomainActive|-|False|
|TypeItemUId|TYPE_UID|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME|OneWayFromSourceRequired|Unknown|typeItemName|-|False|
|IsTypeItemActive|ISACTIVE_TYPE|OneWayFromSourceRequired|Unknown|isTypeItemActive|-|False|
|TypeFunctionName|TYPE_FUNCTION_NAME|OneWayFromSourceRequired|Unknown|typeFunctionName|-|False|
|IsTypeFunctionActive|ISACTIVE_TYPE_FUNCTION|OneWayFromSourceRequired|Unknown|isTypeFunctionActive|-|False|
|ParentUId|ELEMENT_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|ParentName|ELEMENT_NAME_PARENT|OneWayFromSourceRequired|Unknown|parentName|-|False|
|IsParentActive|ISACTIVE_ELEMENT_PARENT|OneWayFromSourceRequired|Unknown|isParentActive|-|False|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|RlsOwner|RLS_OWNER|TwoWayRequired|Unknown|rlsOwner|-|True|
|RlsMask|RLS_MASK|TwoWayRequired|Unknown|rlsMask|-|True|
|RecordStatus|RECORD_STATUS|OneWayFromSourceRequired|Unknown|recordStatus|-|False|
|GlobalProperties|None|None|Unknown|-|-|N/A|
|PrivatePropertyKeys|None|None|Unknown|-|-|N/A|
|PrivateProperties|None|None|Unknown|-|-|N/A|
|ElementEdges|None|None|Unknown|-|-|N/A|
|GlobalPropertyEdges|None|None|Unknown|-|-|N/A|
|PrivatePropertyEdges|None|None|Unknown|-|-|N/A|
|LabelEdges|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
