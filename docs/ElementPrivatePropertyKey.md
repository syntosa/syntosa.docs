# Syntosa.Core.ObjectModel.ElementPrivatePropertyKey
** need class/table description **
DataTable: ELEMENT_PRIVATEPROPERTY_KEY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this PrivatePropertyKey.|Int32|True|True|
|UId|The unique identifier of this PrivatePropertyKey.|Guid|True|True|
|Name|Alias for TypeName field; gets/sets TypeName.|String|True|True|
|IsAutoCollect|Indicates if element data is collected though manual input (AutoCollect=false) or automated processes (AutoCollect=true).|Boolean|True|True|
|ModuleUIdAutoCollect|Foreign key to Module table.  Indicates 'owner' of AutoCollect data; default to Syntosa if IsAutoCollect=false to maintain queryability of data.|Guid|True|True|
|SortOrder|Used to manually specify item's position in a list; overrides default alpha sort order.|Int32|True|True|
|ElementUId|Foreign key to PrivatePropertyKey table.  The PrivatePropertyKey which owns this GlobalProperty.|Guid|True|True|
|ElementName|** need property description **|String|True|False|
|IsElementActive|** need property description **|Boolean|True|False|
|TypeKeyUId|Foreign key to Type table.  The type classifier for the PrivatePoperty Key.|Guid|True|True|
|TypeKeyName|The name of the type for this PrivatePropertyKey Key.|String|True|True|
|TypeValueUId|Foreign key to Type table.  The type classifier for the PrivatePoperty Value.|Guid|True|True|
|TypeValueName|** need property description **|String|True|False|
|TypeKeyValueName|The name of the type for this PrivatePropertyKey Key.|String|True|False|
|TypeUnitUId|Foreign key to Type_Unit table.  The Unit for the PrivatePoperty.|Guid|True|True|
|ParentUId|Self-referencing foreign key to the parent PrivatePropertyKey for this PrivatePropertyKey.|Guid|True|True|
|Children|A list of the child Elements of this PrivatePropertyKey.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|AutoCollectModuleName|** need property description **|String|True|False|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|DependentIds|** need property description **|List`1|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|ELEMENT_PRIVATEPROPERTY_KEY_PK|OneWayFromSourceRequired|Pk|privatePropertyKeyId|SqlDbType.Int|False|
|UId|ELEMENT_PRIVATEPROPERTY_KEY_UID|TwoWayRequired|UId|privatePropertyKeyUId|SqlDbType.UniqueIdentifier|False|
|Name|-|None|Unknown|name|-|N/A|
|IsAutoCollect|ISAUTOCOLLECT|TwoWayRequired|Unknown|isAutoCollect|-|False|
|ModuleUIdAutoCollect|MODULE_UID_AUTOCOLLECT|TwoWayRequired|Unknown|moduleUIdAutoCollect|-|False|
|SortOrder|UISORT_ORDER|TwoWayRequired|Unknown|sortOrder|-|False|
|ElementUId|ELEMENT_UID|TwoWayRequired|Unknown|elementUId|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|TypeKeyUId|TYPE_UID_KEY|TwoWayRequired|Unknown|typeKeyUId|-|False|
|TypeKeyName|TYPE_UID_KEY_NAME|OneWayFromSourceOptional|Name|typeKeyName|-|False|
|TypeValueUId|TYPE_UID_VALUE|TwoWayRequired|Unknown|typeValueUId|-|False|
|TypeValueName|TYPE_UID_VALUE_NAME|OneWayFromSourceRequired|Unknown|typeValueName|-|False|
|TypeKeyValueName|-|None|Unknown|typeKeyValueName|-|N/A|
|TypeUnitUId|TYPE_UNIT_UID|TwoWayRequired|Unknown|typeUnitUId|-|False|
|ParentUId|ELEMENT_PRIVATEPROPERTY_KEY_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|AutoCollectModuleName|MODULE_NAME_AUTOCOLLECT|OneWayFromSourceRequired|Unknown|autoCollectModuleName|-|False|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|DependentIds|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
