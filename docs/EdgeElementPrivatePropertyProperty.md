# Syntosa.Core.ObjectModel.EdgeElementPrivatePropertyProperty
** need class/table description **
DataTable: EDGE_ELEMENT_PRIVATEPROPERTY_XREF_PROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|EdgeType|** need property description **|SyntosaRecordType|True|False|
|EdgeUId|** need property description **|Guid|True|True|
|IsActiveTypeEdgeProperty|** need property description **|Boolean|True|False|
|ElementUId|** need property description **|Guid|True|False|
|ElementPrivatePropertyUId|** need property description **|Guid|True|False|
|ElementUIdDriver|** need property description **|Boolean|True|False|
|TypeNameEdge|** need property description **|String|True|False|
|IsActiveTypeEdge|** need property description **|Boolean|True|False|
|ElementName|** need property description **|String|True|False|
|IsActiveElement|** need property description **|Boolean|True|False|
|TypeUIdElement|** need property description **|Guid|True|False|
|IsActiveElementType|** need property description **|Boolean|True|False|
|TypeNameElement|** need property description **|String|True|False|
|ElementPrivatePropertyKeyUId|** need property description **|Guid|True|False|
|PrivateAttribute|** need property description **|String|True|False|
|ElementUIdPrivateProperty|** need property description **|Guid|True|False|
|TypeUIdKeyPrivateProperty|** need property description **|Guid|True|False|
|TypeUIdValuePrivateProperty|** need property description **|Guid|True|False|
|TypeNameKeyPrivateProperty|** need property description **|Guid|True|False|
|IsActiveTypeKeyPrivateProperty|** need property description **|Boolean|True|False|
|TypeNameValuePrivateProperty|** need property description **|Guid|True|False|
|IsActiveTypeValuePrivateProperty|** need property description **|Boolean|True|False|
|TypeUnitNamePrivateProperty|** need property description **|String|True|False|
|IsActiveTypeUnitPrivateProperty|** need property description **|Boolean|True|False|
|ElementNamePrivateProperty|** need property description **|String|True|False|
|IsActivePrivatePropertyElement|** need property description **|Boolean|True|False|
|TypeUIdPrivatePropertyElement|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|Attribute|** need property description **|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_PRIVATEPROPERTY_XREF_PROPERTY_PK|OneWayFromSourceRequired|Pk|edgeElementPrivatePropId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_PRIVATEPROPERTY_XREF_PROPERTY_UID|TwoWayRequired|UId|edgeElementPrivatePropUId|SqlDbType.UniqueIdentifier|False|
|EdgeType|None|None|Unknown|-|-|N/A|
|EdgeUId|EDGE_ELEMENT_PRIVATEPROPERTY_XREF_UID|TwoWayRequired|Unknown|edgeUId|-|False|
|IsActiveTypeEdgeProperty|ISACTIVE_TYPE_EDGE_PROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeEdgeProperty|-|False|
|ElementUId|ELEMENT_UID|OneWayFromSourceRequired|Unknown|elementUId|-|False|
|ElementPrivatePropertyUId|ELEMENT_PRIVATEPROPERTY_UID|OneWayFromSourceRequired|Unknown|elementPrivatePropertyUId|-|False|
|ElementUIdDriver|ELEMENT_UID_DRIVER|OneWayFromSourceRequired|Unknown|elementUIdDriver|-|False|
|TypeNameEdge|TYPE_NAME_EDGE|OneWayFromSourceRequired|Unknown|typeNameEdge|-|False|
|IsActiveTypeEdge|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isActiveTypeEdge|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsActiveElement|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isActiveElement|-|False|
|TypeUIdElement|TYPE_UID_ELEMENT|OneWayFromSourceRequired|Unknown|typeUIdElement|-|False|
|IsActiveElementType|ISACTIVE_ELEMENT_TYPE|OneWayFromSourceRequired|Unknown|isActiveElementType|-|False|
|TypeNameElement|TYPE_NAME_ELEMENT|OneWayFromSourceRequired|Unknown|typeNameElement|-|False|
|ElementPrivatePropertyKeyUId|ELEMENT_PRIVATEPROPERTY_KEY_UID|OneWayFromSourceRequired|Unknown|elementPrivatePropertyKeyUId|-|False|
|PrivateAttribute|PRIVATE_ATTRIBUTE|OneWayFromSourceRequired|Unknown|privateAttribute|-|False|
|ElementUIdPrivateProperty|ELEMENT_UID_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|elementUIdPrivateProperty|-|False|
|TypeUIdKeyPrivateProperty|TYPE_UID_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdKeyPrivateProperty|-|False|
|TypeUIdValuePrivateProperty|TYPE_UID_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdValuePrivateProperty|-|False|
|TypeNameKeyPrivateProperty|TYPE_NAME_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeNameKeyPrivateProperty|-|False|
|IsActiveTypeKeyPrivateProperty|ISACTIVE_TYPE_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeKeyPrivateProperty|-|False|
|TypeNameValuePrivateProperty|TYPE_NAME_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeNameValuePrivateProperty|-|False|
|IsActiveTypeValuePrivateProperty|ISACTIVE_TYPE_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeValuePrivateProperty|-|False|
|TypeUnitNamePrivateProperty|TYPE_UNIT_NAME_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUnitNamePrivateProperty|-|False|
|IsActiveTypeUnitPrivateProperty|ISACTIVE_TYPE_UNIT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeUnitPrivateProperty|-|False|
|ElementNamePrivateProperty|ELEMENT_NAME_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|elementNamePrivateProperty|-|False|
|IsActivePrivatePropertyElement|ISACTIVE_PRIVATEPROPERTY_ELEMENT|OneWayFromSourceRequired|Unknown|isActivePrivatePropertyElement|-|False|
|TypeUIdPrivatePropertyElement|TYPE_UID_PRIVATEPROPERTY_ELEMENT|OneWayFromSourceRequired|Unknown|typeUIdPrivatePropertyElement|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE_PROPERTY|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE_PROPERTY|OneWayFromSourceRequired|Name|typeItemName|-|False|
|Attribute|EDGE_PROPERTY_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
