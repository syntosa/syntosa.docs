# Syntosa.Core.ObjectModel.EdgePrivatePropertyPrivateProperty
** need class/table description **
DataTable: EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|LeftRecordType|** need property description **|SyntosaRecordType|True|False|
|LeftUId|** need property description **|Guid|True|True|
|SourcePrivatePropertyUId|** need property description **|Guid|True|True|
|SourceKeyUId|** need property description **|Guid|True|False|
|IsSourceKeyActive|** need property description **|Boolean|True|False|
|PrivateAttributeSource|** need property description **|String|True|False|
|SourceKeyTypeUId|** need property description **|Guid|True|False|
|SourceKeyTypeName|** need property description **|String|True|False|
|IsSourceKeyTypeActive|** need property description **|Boolean|True|False|
|SourceValueTypeUId|** need property description **|Guid|True|False|
|SourceValueTypeName|** need property description **|String|True|False|
|IsSourceKeyValueActive|** need property description **|Boolean|True|False|
|SourceTypeUnitUId|** need property description **|Guid|True|False|
|IsSourceTypeUnitActive|** need property description **|Boolean|True|False|
|SourceElementUId|** need property description **|Guid|True|False|
|SourceElementName|** need property description **|String|True|False|
|IsSourceElementActive|** need property description **|Boolean|True|False|
|SourceElementTypeUId|** need property description **|Guid|True|False|
|RightRecordType|** need property description **|SyntosaRecordType|True|False|
|RightUId|** need property description **|Guid|True|True|
|TargetPrivatePropertyUId|** need property description **|Guid|True|True|
|TargetKeyUId|** need property description **|Guid|True|False|
|IsTargetKeyActive|** need property description **|Boolean|True|False|
|TargetPrivateAttribute|** need property description **|String|True|False|
|TargetKeyTypeUId|** need property description **|Guid|True|False|
|TargetKeyTypeName|** need property description **|String|True|False|
|IsTargetKeyTypeActive|** need property description **|Boolean|True|False|
|TargetValueTypeUId|** need property description **|Guid|True|False|
|TargetValueTypeName|** need property description **|String|True|False|
|IsTargetValueTypeActive|** need property description **|Boolean|True|False|
|TargetUIdTypeUnit|** need property description **|Guid|True|False|
|IsTargetTypeUnitActive|** need property description **|Boolean|True|False|
|TargetElementUId|** need property description **|Guid|True|False|
|TargetElementName|** need property description **|String|True|False|
|IsTargetElementActive|** need property description **|Boolean|True|False|
|TargetElementTypeUId|** need property description **|Guid|True|False|
|IsLeftUIdSource|** need property description **|Boolean|True|True|
|EdgeProperties|** need property description **|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|** need property description **|IList|True|False|
|UdtPrivatePropertyUId|Internal use only, supports UDT for TVP calls only.|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|IsTypeItemActive|** need property description **|Boolean|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF_PK|OneWayFromSourceRequired|Pk|edgePrivatePrivateId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_PRIVATEPROPERTY_PRIVATEPROPERTY_XREF_UID|TwoWayRequired|UId|edgePrivatePrivateUId|SqlDbType.UniqueIdentifier|False|
|LeftRecordType|None|None|Unknown|-|-|N/A|
|LeftUId|None|None|Unknown|-|-|N/A|
|SourcePrivatePropertyUId|ELEMENT_PRIVATEPROPERTY_UID_SOURCE|TwoWayRequired|Unknown|sourcePrivatePropertyUId|-|False|
|SourceKeyUId|ELEMENT_PRIVATEPROPERTY_KEY_UID_SOURCE|OneWayFromSourceRequired|Unknown|sourceKeyUId|-|False|
|IsSourceKeyActive|ISACTIVE_ELEMENT_PRIVATEPROPERTY_KEY_SOURCE|OneWayFromSourceRequired|Unknown|isSourceKeyActive|-|False|
|PrivateAttributeSource|PRIVATE_ATTRIBUTE_SOURCE|OneWayFromSourceRequired|Unknown|privateAttributeSource|-|False|
|SourceKeyTypeUId|TYPE_UID_KEY_SOURCE|OneWayFromSourceRequired|Unknown|sourceKeyTypeUId|-|False|
|SourceKeyTypeName|TYPE_NAME_KEY_SOURCE|OneWayFromSourceRequired|Unknown|sourceKeyTypeName|-|False|
|IsSourceKeyTypeActive|ISACTIVE_TYPE_KEY_SOURCE|OneWayFromSourceRequired|Unknown|isSourceKeyTypeActive|-|False|
|SourceValueTypeUId|TYPE_UID_VALUE_SOURCE|OneWayFromSourceRequired|Unknown|sourceValueTypeUId|-|False|
|SourceValueTypeName|TYPE_NAME_VALUE_SOURCE|OneWayFromSourceRequired|Unknown|sourceValueTypeName|-|False|
|IsSourceKeyValueActive|ISACTIVE_VALUE_KEY_SOURCE|OneWayFromSourceRequired|Unknown|isSourceKeyValueActive|-|False|
|SourceTypeUnitUId|TYPE_UNIT_UID_SOURCE|OneWayFromSourceRequired|Unknown|sourceTypeUnitUId|-|False|
|IsSourceTypeUnitActive|ISACTIVE_TYPE_UNIT_SOURCE|OneWayFromSourceRequired|Unknown|isSourceTypeUnitActive|-|False|
|SourceElementUId|ELEMENT_UID_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementUId|-|False|
|SourceElementName|ELEMENT_NAME_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementName|-|False|
|IsSourceElementActive|ISACTIVE_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|isSourceElementActive|-|False|
|SourceElementTypeUId|TYPE_UID_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementTypeUId|-|False|
|RightRecordType|None|None|Unknown|-|-|N/A|
|RightUId|None|None|Unknown|-|-|N/A|
|TargetPrivatePropertyUId|ELEMENT_PRIVATEPROPERTY_UID_TARGET|TwoWayRequired|Unknown|targetPrivatePropertyUId|-|False|
|TargetKeyUId|ELEMENT_PRIVATEPROPERTY_KEY_UID_TARGET|OneWayFromSourceRequired|Unknown|targetKeyUId|-|False|
|IsTargetKeyActive|ISACTIVE_ELEMENT_PRIVATEPROPERTY_KEY_TARGET|OneWayFromSourceRequired|Unknown|isTargetKeyActive|-|False|
|TargetPrivateAttribute|PRIVATE_ATTRIBUTE_TARGET|OneWayFromSourceRequired|Unknown|targetPrivateAttribute|-|False|
|TargetKeyTypeUId|TYPE_UID_KEY_TARGET|OneWayFromSourceRequired|Unknown|targetKeyTypeUId|-|False|
|TargetKeyTypeName|TYPE_NAME_KEY_TARGET|OneWayFromSourceRequired|Unknown|targetKeyTypeName|-|False|
|IsTargetKeyTypeActive|ISACTIVE_TYPE_KEY_TARGET|OneWayFromSourceRequired|Unknown|isTargetKeyTypeActive|-|False|
|TargetValueTypeUId|TYPE_UID_VALUE_TARGET|OneWayFromSourceRequired|Unknown|targetValueTypeUId|-|False|
|TargetValueTypeName|TYPE_NAME_VALUE_TARGET|OneWayFromSourceRequired|Unknown|targetValueTypeName|-|False|
|IsTargetValueTypeActive|ISACTIVE_TYPE_VALUE_TARGET|OneWayFromSourceRequired|Unknown|isTargetValueTypeActive|-|False|
|TargetUIdTypeUnit|TYPE_UNIT_UID_TARGET|OneWayFromSourceRequired|Unknown|targetUIdTypeUnit|-|False|
|IsTargetTypeUnitActive|ISACTIVE_TYPE_UNIT_TARGET|OneWayFromSourceRequired|Unknown|isTargetTypeUnitActive|-|False|
|TargetElementUId|ELEMENT_UID_TARGET|OneWayFromSourceRequired|Unknown|targetElementUId|-|False|
|TargetElementName|ELEMENT_NAME_TARGET|OneWayFromSourceRequired|Unknown|targetElementName|-|False|
|IsTargetElementActive|ISACTIVE_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|isTargetElementActive|-|False|
|TargetElementTypeUId|TYPE_UID_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|targetElementTypeUId|-|False|
|IsLeftUIdSource|None|None|Unknown|-|-|N/A|
|EdgeProperties|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|None|None|Unknown|-|-|N/A|
|UdtPrivatePropertyUId|ELEMENT_PRIVATEPROPERTY_UID|None|Unknown|udtPrivatePropertyUId|-|N/A|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE|OneWayFromSourceRequired|Name|typeItemName|-|False|
|IsTypeItemActive|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isTypeItemActive|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
