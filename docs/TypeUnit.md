# Syntosa.Core.ObjectModel.TypeUnit
** need class/table description **
DataTable: TYPE_UNIT

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this TypeUnit.|Int32|True|True|
|UId|The unique identifier of this TypeUnit.|Guid|True|True|
|Name|The name of this TypeUnit.|String|True|True|
|Description|The friendly description of this TypeUnit.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|IsBuiltIn|Indicates items which are integral to system function.  IsBuiltIn=true items cannot be deleted.|Boolean|True|True|
|ModuleUId|Foreign key to Module table; the Module for this TypeUnit.|Guid|True|True|
|ModuleName|** need property description **|String|True|False|
|IsModuleActive|** need property description **|Boolean|True|False|
|ParentUId|Self-referencing foreign key to the parent TypeUnit for this TypeUnit.|Guid|True|True|
|ParentName|** need property description **|String|True|False|
|IsParentActive|** need property description **|Boolean|True|False|
|Children|A list of the child TypeUnits of this TypeUnit.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|TYPE_UNIT_PK|OneWayFromSourceRequired|Pk|typeUnitId|SqlDbType.Int|False|
|UId|TYPE_UNIT_UID|TwoWayRequired|UId|typeUnitUId|SqlDbType.UniqueIdentifier|False|
|Name|TYPE_UNIT_NAME|TwoWayRequired|Name|typeUnitName|-|False|
|Description|TYPE_UNIT_DESC|TwoWayRequired|Unknown|typeUnitDesc|-|False|
|IsActive|ISACTIVE|TwoWayOptional|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayOptional|Unknown|isBuiltIn|-|False|
|ModuleUId|MODULE_UID|TwoWayRequired|Unknown|moduleUId|-|False|
|ModuleName|MODULE_NAME|OneWayFromSourceRequired|Unknown|moduleName|-|False|
|IsModuleActive|ISACTIVE_MODULE|OneWayFromSourceRequired|Unknown|isModuleActive|-|False|
|ParentUId|TYPE_UNIT_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|ParentName|TYPE_UNIT_NAME_PARENT|OneWayFromSourceRequired|Unknown|parentName|-|False|
|IsParentActive|ISACTIVE_TYPE_UNIT_PARENT|OneWayFromSourceRequired|Unknown|isParentActive|-|False|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
